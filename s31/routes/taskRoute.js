//Contains all the endpoints for our application
//We seperate the routes such that "app.js" only contains information on the server

const express = require("express")

//Create a Router instance that functions as middleware and routing system
//Allow access to HTTP method middlwares that makes it easier to create routes for our application
const router = express.Router();

const taskController = require("../controllers/taskControllers")

//[Routes]
//Route for getting all the tasks

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

//Route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//Route for Deleting a task
	//call out the routing component to register a brand new endpoint.
	//When integrating a path variable whitin the URI, you are also changing the behaviour of the path from STATIC TO DYNAMIC.
	//2 Types of End Points
		//Static Route
			//->unchanging, fixed, constant, steady
		//Dynamic Route
			//> interchangeable or NOT FIXED,	 
router.delete('/:task', (req, res) => {
	//identify the task to be executed within this endpoint
	//call out the proper function for this route and identify the source/provider of the function.
	//DETERMINE wheter the value inside the path varaiable is transmitted to the server.
	console.log(req.params.task);
	//place the value of the path variable inside its own container.
	let taskID = req.params.task;
	// res.send('Hello from delete'); //this is only to check if the setup is correct
	
	//retrive the identity of the task by inserting the ObjectId of the resourse.
	//make sure to pass down the identity of the task using the bproper reference (objectId), howeve this time we are going to include the information as a path variable.
	//Path variable -> this will allow us to insert data within the scope of the URL.
		//what is variable ? -> container, storage of inforamtion
		//When is a path variable useful? 
			//When inserting only a single piece of information.
			//this is also useful when passing down information to REST API method that does not include a BODY section like 'GET'.
	//upon executing this method a promise will be initialized so we need to handle the result of the promise
	taskController.deleteTask(taskID).then(resultNgDelete => res.send(resultNgDelete));
});

//Route for updating the task status (Pending -> Complete)
	//Lets create a dynamic endpoint for this brand new route.
router.put('/:task', (req, res) => {
	//check if you are able to acquire the values inside the path variables.
	//console.log(req.params.task); //check if the value inside the parameter of the route is properly transmitted to the server.
	let idNiTask = req.params.task //this is declared to simplify the means of calling out the value of the path variable key.

	//identify the business logic behind this task inside the controller module.
	//call the intended controller to execute the process make sure to identify the provider,source of the function.
	//after invoking the controller method handle the outcome
	taskController.taskCompleted(idNiTask).then(outcome => res.send(outcome));
});

//Route for updating task status (Completed -> Pending)
	//this will be a counter procedure for the previous task.
router.put('/:task/pending', (req, res) => {
	let id = req.params.task;
	//console.log(id); //test the initial setup

	//declare the business logic aspect of this brand new task in our app.
	//invoke the task you want to execute in this route.
	taskController.taskPending(id).then(outcome => {
		res.send(outcome);
	});
});



//Use "modules.exports" to export the router object to use in the "app.js"
module.exports = router;