//Mini Activity:
/*
	Setup a basic Express JS server
*/
	//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");
	const taskRoute = require("./routes/taskRoute");

//[SECTION] Server Setup
	const app = express();
	const port = 4000; 

	app.use(express.json());
	app.use(express.urlencoded({extended :true}));


//[SECTION] Database Connection
	mongoose.connect('mongodb+srv://Dexter_Ladines:1234@cluster0.xggl7.mongodb.net/toDo176?retryWrites=true&w=majority',
	{

		useNewUrlParser:true,
		useUnifiedTopology:true
	});
	
	let db = mongoose.connection;
	db.on('error', console.error.bind(console,"Connection Error"));
	db.once('open',()=>console.log("Connected to MongoDB"));

//[SECTION] Routing System
//Add the task route
//localhost:4000/tasks/
	app.use("/tasks", taskRoute);

//[SECTION] Entry Point Response
app.listen(port, () => console.log(`Server running at port ${port}`));