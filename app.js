//[SECTION] Dependencies and Modules
	const express = require("express");
	const mongoose = require("mongoose");

//[SECTION] Server Setup
	//establish a connection
	const app = express();
	//define a path/address
	const port = 4000; //Assign the port 4000 to avoid conflict in the future with frontend applications that run on port 3000


//[SECTION] Databasse Connection
	//Connect to MongoDB Atlas
	//Get the credential of your atlas user.
	mongoose.connect('mongodb+srv://Dexter_Ladines:1234@cluster0.xggl7.mongodb.net/toDo176?retryWrites=true&w=majority',{

		//options to add to avoid depreciation warnings because of mongoose/mongodb update.
		useNewUrlParser:true,
		useUnifiedTopology:true
	

	});
	
	//Create notifications if the connection to the db is a success or a failure:
	let db = mongoose.connection;
	//Let's add an on() method from our mongoose connection to show if the connection has succedded or failed in both the terminal and in the browser for our client.
	db.on('error', console.error.bind(console,"Connection Error"));
	//once the connection is open and successful, we will output a message in thr terminal:
	db.once('open',()=>console.log("Connected to MongoDB"));

	//Middleware - A middleware, in expressjs context, are methods, functions that acts and adds features to our application.
	app.use(express.json());



	//Schema
	//Before we can create documents from our api to save into our database, we first have to determine the structure of the documents to be written in our database.
	//This is to ensure the consistency of our documents and avoids future errors

	//Schema acts as a blueprint for our data/documents

	//Schema() constuctor from our mongoose to create a neq schema object
	const taskSchema = new mongoose.Schema({

		/*
			Define the fields for our documents.
			We will also be able to determine the eppropriate data type of the values.

		*/
		name: String,
		status: String
	})

	//Mongoose Model
	/*
		Models are used to connect your api to the corressponding collection in your database. It is a represnetation of your collection.

		Models uses schemas to create object that correspond to thr chema. By default, when creating the collection from your model, the collection name is pluralized.

		mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)
	*/

	const Task = mongoose.model("task",taskSchema);

	//Post route to create a new task
	app.post('/tasks',(req,res)=>{

		//When creating a new post/put pr any route that requires data from the client, first console log your req.body or any part of the request that contains the data.
		//console.log(req.body);

		//Creating a new task documents bu using the constructor of our Task model. This constructor should follow the schema of the model.
		let newTask = new Task({

			name: req.body.name,
			status: req.body.status
		})

		//.save() method from an object created by a model.
		//save()method will allow us to save or document by connecting to our collection via our model.

		//save() has 2 approaches: 
		//1. we can add an anonymous function to handle the created document or error.
		//2. We can add .then() chain will allow us to handel errors and created documents in separte functions.
		// newTask.save((error,savedTask)=>{

		// 	if (error){
		// 		res.send("Document creation failed.");
		// 	} else {
		// 		res.send(savedTask)
		// 	}
		// })
		//.then() and .catch() chain
		//.then() is used to handle the proper result/returned value of a function.If the function properly returns a value, we can  a separate function to handle it.
		//.catch() is used to handle/catch the error from the use of a function. So that if an error occurs, we can handle the error separately.

		newTask.save()
		.then(result => res.send({message: "Document Creation Successful"}))
		.catch(error => res.send({message: "Error in Document Creation"}));
	}) 

	//GET Method Request to Retrieve ALL Task Documents form our Collection

	app.get('/tasks',(req,res)=>{

		//TO query using mongoose, first access the model of the collection you want to manipulate
		//Model.find() in mongoose is similar in function to mongDB's db.collection.find()
		//mongodb - db.tasks.
		Task.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));

	})

//new schema

	const sampleSchema = new mongoose.Schema({

		name:String,
		isActive: Boolean

	})

	const Sample = mongoose.model("samples", sampleSchema);

	app.post('/samples',(req,res)=>{
		
		let newSample = new Sample({

			name: req.body.name,
			isActive: req.body.isActive
		})

		newSample.save((error,savedSample)=>{

			if(error){
				res.send(error);
			} else {
				res.send(savedSample);
			}

		})
	})

//GET 

app.get('/samples',(req,res)=>{

		Sample.find({})
		.then(result => res.send(result))
		.catch(err => res.send(err));

	})






/*Activity:       

Create a new schema for User. It should have the following fields:
                username,password.
            The data types for both fields is String.*/

const userSchema = new mongoose.Schema({
	username: String,
	password: String
})

	


// Create a new model out of your schema and save it in a variable called User

const User = mongoose.model("user",userSchema);


/* Create a new POST method route to create a new user document:
	-endpoint: "/users"
	-This route should be able to create a new user document.
	-Then, send the result in the client.
	-Catch an error while saving, send the error in the client.*/
app.post('/users',(req,res)=>{

		let newUser = new User({

			username: req.body.username,
			password: req.body.password
		})

			newUser.save()
			.then(result => res.send({message: "Document Creation Successful"}))
			.catch(error => res.send({message: "Error in Document Creation"}));
		}) 




  /*Create a new GET method route to retrieve all user documents:
    -endpoint: "/users"
    -Then, send the result in the client.
    -Catch an error, send the error in the client.
*/

app.get('/users',(req,res)=>{

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

})














/*

Create a new GET request method route to get all sample documents.Send the array of sample documents in our postman client
else, catch the error and send the error in the client*/

//new schema

	/*const manggagamitSchema = new mongoose.Schema({

		username:String,
		isAdmin:Boolean
	})

	const Manggagamit = mongoose.model("manggagamit", manggagamitSchema);

	app.post("/mgaManggagamit", (req,res)=>{

		let newManggagamit = new Manggagamit({

			username: req.body.username,
			isAdmin:req.body.isAdmin
		})

		newManggagamit.save((savedManggagamit,error)=>{
			if(error){
				res.send(error);
			} else {
				res.send(savedManggagamit);
			}
		})
	})
*/

//[SECTION] Entry Point Response
	//bind the connection to the designated port
	app.listen(port, () => console.log(`Server running at port ${port}`));